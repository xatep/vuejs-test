import _ from 'lodash';
import axios from 'axios';
import Vue from 'vue/dist/vue.js';
// import data from './data';

const vueApp = () => {
  Vue.component('x-article', {
    template: '#article-template',
    props: {
      title: String,
      imgUrl: {
        type: String,
        default: '/images/stub_350x150.png',
      },
    },
  });

  new Vue({
    el: '#app',
    data() {
      return {
        data: [],
        errors: [],
        filter: {},
        urlParams: {},
      };
    },

    mounted: function() {
      axios.get('https://jsonplaceholder.typicode.com/posts').then(response => {
        this.data = response.data;
      });

      this.urlParams = this.getUrlSearchParams();

      this.filter = {
        userId: this.urlParams.get('userId'),
        title: this.urlParams.get('title'),
      };
    },

    computed: {
      users() {
        return _.uniq(_.map(this.data, 'userId'));
      },

      articles() {
        return this.getFilteredArticles();
      },
    },

    methods: {
      getFilteredArticles() {
        return this.getPublishedArticles().filter(article => {
          const urlParams = this.getUrlSearchParams();

          const titleFilter = urlParams.get('title');
          const userIdFilter = urlParams.get('userId');

          if (titleFilter && article.title.search(titleFilter) === -1) {
            return false;
          }

          if (userIdFilter && parseInt(article.userId) !== parseInt(userIdFilter)) {
            return false;
          }

          return true;
        });
      },

      getUrlSearchParams() {
        return new URLSearchParams(window.location.search);
      },

      getPublishedArticles() {
        return this.data.filter(article => !article.status || article.status === 'published');
      },

      validateForm(event) {
        if (/^[A-Za-z0-9\s?!.,"']*$/.test(this.filter.title)) {
          return true;
        }

        this.errors.push('Search field contains incorrect symbols.');
        event.preventDefault();
      },
    },
  });
};

const init = () => {
  vueApp();
};

export default init;
